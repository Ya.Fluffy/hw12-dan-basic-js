// ? Functions, arrow function ...

//  Теоретичні питання
//  1. Як можна сторити функцію та як ми можемо її викликати?
// За допомогою ключового слова function назва функцїї(){}
// Щоб викликати - написати назву функціїї з дужками - назваФункції()

//  2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
//   Оператор return повертає назад щось, якщо вказуємо що треба повернути, або просто зупиняє подальше виконання функції, якщо ми вказуємо тільки return

//  3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
//-Параметри, це змінні, які оголошуються і визначенні функції
// -Аргументи передаються у функцію при її виклику.

//  4. Як передати функцію аргументом в іншу функцію?
// Треба передати в функцію назву другох функції, як аргумент. такі функції, що передаються як аргумент, і викликаються всередні іншої функції називаються callback функціями або функціями зворотнього виклику
 function foo1(name, n){
    return name(n)
 }
function foo2(n){
    return 2*n
}
console.log(foo1(foo2, 5))


//========================
//  Практичні завдання
//  1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
let number1 = 35;
let number2 = 5;
function division(number1, number2) {
  return (result = number1 / number2);
}
console.log(division(number1, number2));


//===========================
//  2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.


let number1User;
let number2User;

do {
  number1User = prompt("Enter first number");
  number1 = Number(number1User);
} while (isNaN(number1User) || number1User === "" || number1User === null);
console.log("number1", number1User);

do {
  number2User = prompt("Enter second number");
  number2User = Number(number2User);
} while (isNaN(number2User) || number2User === "" || number2User === null);
console.log("number2", number2User);

do {
  mathAction = prompt("Math action");
  if (
    mathAction !== "+" &&
    mathAction !== "/" &&
    mathAction !== "-" &&
    mathAction !== "*"
  ) {
    alert("Такої операції не існує");
  }
  console.log("Entered action: ", mathAction);
} while (
  mathAction !== "+" &&
  mathAction !== "/" &&
  mathAction !== "-" &&
  mathAction !== "*"
);

console.log("You selected: " + mathAction);

function math(number1, number2, mathAction) {
  let result;
  switch (mathAction) {
    case "/":
      result = number1 / number2;
      break;
    case "+":
      result = number1 + number2;
      break;
    case "-":
      result = number1 - number2;
      break;
    case "*":
      result = number1 * number2;
      break;
    default:
        result = 'Invalid operation';
      break;
  }
  return result;
}
console.log(math(number1, number2, mathAction));


//========================
// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

let modalNumber 
do {
    modalNumber = prompt('Enter number')
    modalNumber = Number(modalNumber);
  } while (isNaN(modalNumber) || modalNumber === "" || modalNumber === null);
  console.log("modalNumber", modalNumber);

const  factorial = (number) => {
    let result = 1
    for(let i = number; i>0; i--){
        console.log(i)
        result *= i
    }
    return result
}
console.log(factorial(modalNumber))


const  factorial1 = (number) => {
    return number ===0? 1:  number * factorial1(number - 1);
}
console.log(factorial1(modalNumber))











// function sum(a,b){
//     let result = a+b
//     return result
// }

// console.log(sum(3,5))

// function counter(start, finish, multiple){
//     if(start > finish) {
//         console.log('Рахунок неможливий')
//     } else if(start === finish) {
//         console.log("Рахувати не треба")
//     } else {
//         console.log('Відлік розпочато')
//         for (i = start; i <= finish; i++) {
//             if(i%multiple === 0){
//                 console.log(i)
//             }

//         }
//         console.log("Відлік завершено")
//     }
// }
// counter(25, 77, 5)



//!   foo1()   // error - Cannot access 'foo1' before initialization
(function(){
    let param = "IIFE";
 console.log(param)
})() // викликається тільки в самому верху коду!


const foo1=()=>{
    console.log("function")

}
 let a = foo1()
console.log(a)
console.log(foo1())
console.log(undefined)






