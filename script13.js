const tasks = [
  {
    _id: "5d2ca9e2e03d40b326596aa7",
    completed: true,
    body: "Occaecat non ea quis occaecat ad culpa amet deserunt incididunt elit fugiat pariatur. Exercitation commodo culpa in veniam proident laboris in. Excepteur cupidatat eiusmod dolor consectetur exercitation nulla aliqua veniam fugiat irure mollit. Eu dolor dolor excepteur pariatur aute do do ut pariatur consequat reprehenderit deserunt.\r\n",
    title: "Eu ea incididunt sunt consectetur fugiat non.",
  },
  {
    _id: "5d2ca9e29c8a94095c1288e0",
    completed: false,
    body: "Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n",
    title:
      "Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.",
  },
  {
    _id: "5d2ca9e29c8a94095564788e0",
    completed: false,
    body: "Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n",
    title:
      "Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.",
  },
];

(function (allOfTasks) {
  const objectOfTasks = allOfTasks.reduce((acc, task) => {
    acc[task._id] = task;
    return acc;
  }, {});

  const taskContainer = document.querySelector(".list-group");
  taskContainer.addEventListener('click', onClickHandler);

  const form = document.forms["addTask"];

  const inputTaskTitle = form.elements["title"];
  const inputTaskBody = form.elements["body"];

  form.addEventListener("submit", onFormSubmit);

  const fragment = document.createDocumentFragment();
  renderTasks(objectOfTasks);
  function renderTasks(tasksObject) {
    Object.values(tasksObject).forEach((task) => {
      const taskTemplait = createTaskTemplate(task);
      fragment.append(taskTemplait);
    });
    taskContainer.append(fragment);
  }

  function createTaskTemplate(task) {
    const li = document.createElement("li");
    li.classList.add("list-item");
    li.setAttribute('id', task._id)

    const taskTitle = document.createElement("span");
    taskTitle.innerText = task.title;
    taskTitle.style.fontWeight = "bold";

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete-btn");
    deleteButton.innerText = "Delete task";

    const taskDescription = document.createElement("p");
    taskDescription.classList.add("task-descr");
    taskDescription.innerText = task.body;

    li.append(taskTitle);
    li.append(taskDescription);
    li.append(deleteButton);

    return li;
  }

  function onFormSubmit(event) {
    event.preventDefault();

    const titleValue = inputTaskTitle.value;
    const bodyValue = inputTaskBody.value;
    console.log(titleValue, bodyValue);

    if (!titleValue || !bodyValue) {
      alert("Enter please task title or task body ");
      return;
    }
    const newTask = {
      _id: new Date(),
      title: titleValue,
      body: bodyValue,
      completed: false,
    };

    objectOfTasks[newTask._id] = newTask;

    inputTaskTitle.innerText = "";
    inputTaskBody.innerText = "";

    const taskTemplait = createTaskTemplate(newTask);
    insertTask(taskTemplait);
  }
  function insertTask(task) {
    if (!task) {
      return;
    }
    taskContainer.prepend(task);
  }

  function onClickHandler(event) {
    console.log(event);
    if (event.target.classList.contains("delete-btn")) {
      console.log(event.target);
      const id = event.target.parentElement.id;
console.log('id',event.target.parentElement, id)
      onDeleteTask(id);
    }
  }

  function onDeleteTask(id) {
    console.log('onDeleteTask', id)
    if (!id) {
      return;
    }
    const isDelete = confirm("Do you want delete task?");
    if (!isDelete) {
      return;
    }
    delete objectOfTasks[id]
    onDeleteFromTemplait(id)
  }

function onDeleteFromTemplait(id){
  const task = document.querySelector(`[id="${id}"]`)
  task.remove()

}

})(tasks);
