//todo:  Loops

//  Теоретичні питання
//  1. Що таке цикл в програмуванні?
// Це якісь набір команд, які дозволяють повторно виконувати програму/якийсь блок коду, поки умова є істиною.

//  2. Які види циклів є в JavaScript і які їх ключові слова?
// -for
// - for in
// - for of
// - switch
// - while
// - do while

//  3. Чим відрізняється цикл do while від while?
// do while - виконається мінімум 1 раз, навіть якщо умова не true
// while - виконується, поки умова повертає true



//===============
//  Практичні завдання
//  1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.
let number1;
let number2;
do {
  number1 = prompt("Enter first number");
  number1 = Number(number1);
} while (isNaN(number1) || number1 === "" || number1 === null);
console.log("number1", number1);

do {
  number2 = prompt("Enter second number");
  number2 = Number(number2);
} while (isNaN(number2) || number2 === "" || number2 === null);
console.log("number2", number2);

if (number1 < number2){
    for (let i = number1; i <= number2; i++)
        console.log('number1 < number2', i)
} else {
    for (let i = number2; i <= number1; i++)
        console.log('number1 > number2', i)
}

//===============
//  2. Напишіть програму, яка запитує в користувача число та перевіряє,
//  чи воно є парним числом. Якщо введене значення не є парним числом,
//  то запитуйте число доки користувач не введе правильне значення.
let evenNumber

do {
    evenNumber = prompt('enter even number')
} while ((evenNumber % 2) !== 0 || evenNumber === null || evenNumber === '');

console.log(typeof evenNumber, evenNumber)



















// let nameUser = null

// let outName = nameUser ?? "Dear Friend"

// console.log(outName)

// nameUser = "Piter"

// outName = nameUser ?? "Dear Friend"
// console.log(outName)

// const str = 'test string';

// for (let i = 0; i <str.length; i++){
//     if(i%2 === 0) {
//         console.log(str[i])
//     } else {

//         console.log(str[i].toUpperCase())
//     }

// }

// let sum = prompt("enter sum");
// while (!sum) {sum = prompt("enter sum")}

// let drink = prompt("enter your drink")
// while (!drink) { drink = prompt("enter your drink")}

// drink.toLowerCase()
// console.log(drink)

// const kapuchino = 25;
// const tea = 10;
// const coffe = 25;
// let change = 0;

//   switch (drink) {
//     case "kapuchino":
//       if (sum === kapuchino) {
//         alert(`Your ${drink} is ready. Thanks for sum without change`);
//       } else if (sum > kapuchino) {
//         change = sum - kapuchino;
//         alert(`Your ${drink} is ready. Take your change ${change}`);
//       } else {
//         alert(`Your money not enough`);
//       }
//       break;
//     case "tea":
//       if (sum === tea) {
//         alert(`Your ${drink} is ready. Thanks for sum without change`);
//       } else if (sum > tea) {
//         change = sum - tea;
//         alert(`Your ${drink} is ready. Take your change ${change}`);
//       } else {
//         alert(`Your money not enough`);
//       }
//       break;
//     case "coffe":
//       if (sum === coffe) {
//         alert(`Your ${drink} is ready. Thanks for sum without change`);
//       } else if (sum > coffe) {
//         change = sum - coffe;
//         alert(`Your ${drink} is ready. Take your change ${change}`);
//       } else {
//         alert(`Your money not enough`);
//       }
//       break;
//     default:
//       {
//         alert(`Enter your drink and sum correct`);
//       }
//       break;
//   }
