// debugger
// out: for (let i = 1; i < 4; i++) {
//     for (let j = 1; j < 4; j++) {
//        if(i + j == 3) continue out;
//         console.log(i, j)
//     }
// }

//todo:   OBJECT


//  Теоретичні питання
//  1. Опишіть своїми словами, що таке метод об'єкту
// це функція, яка виконує якийсь дії над обʼєктом

//  2. Який тип даних може мати значення властивості об'єкта?
// - будь-який тип даних, навіть функцію.


//  3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Обʼєкт і масив є посилальним типом даних. тобто при порівнянні двох обʼєктів порівнюється місце/посилання, по якому лежить/зберігається цей обʼєкт. Тобто змінній присвоюється посилання на зберігання обʼєкта в памʼяті.
 const object = {
    aa: "nn",
    dd: 55
 }
  const object2 = object
  object2.rr = "yy"
console.log(object === object2) //true , бо це один і той же обʼєкт, який має одне посилання на ячейку зберігання.

//  Практичні завдання
//  1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
 const product = {
    name: "name",
    price: 59,
    discount: 5,
 }

 function getPrice(item){
    let sum = (item.price - ((item.price*item.discount)/100)).toFixed(2)
    console.log(`The price of the product: ${sum}`)
 }

 getPrice(product)

//  2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком, 
//  наприклад "Привіт, мені 30 років". 
//  Попросіть користувача ввести своє ім'я та вік 
//  за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.
// console.log( 'fhf'> 0, isNaN('3'))
let name1;
let age ;
do{
    name1 = prompt('Enter your name')
    // console.log(typeof name, name)
} while( name1 === null || name1 === '' || !isNaN(Number(name1)) )

do{
    age  = prompt('Enter your age')
    age = Number(age)
    console.log(typeof age, age)
} while(age === null || age === 0 || isNaN(age))



const user = {
    userName: name1,
    userAge: age
}
function userGreetings(item){
    alert(`Hello! I am ${item.userAge} years old`)
}

userGreetings(user)

// 3.Опціональне. Завдання:
// Реалізувати повне клонування об'єкта. 

// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.

const obj = {
    name: "Name",
    age: 17,
    placeOfBortn: {
        country: null,
        region: {
            index: null,
            town: null
        }
    }
}


const  getClonObj=(obj)=>{
    let clon = {}
    // console.log('clon', clon)
    for ( let key in obj) {
        // console.log(key)
        // console.log(obj[key])
        clon[key]=obj[key]
        // console.log('clon', clon)
        if( typeof obj[key] === "object"){
            console.log(obj[key])
            getClonObj(obj[key])
        }
    }
    return clon
}

console.log(getClonObj(obj))





















// const user = {
//     name: "Nadiia",
//     lastName: "Yena",
//     profesion: "fullstack",
//     sayHi: ()=>console.log("Привіт")
// }
// user.sayHi()

// let car = {
//         name: 'Lexus',
//         age: 10,
//         lastService: '6 month',
//         create: 2008,
//         needRepair: false
//     }

// function check(obj){
//     let service = parseInt(car.lastService)
//     console.log(service)

// if (service > 5){
//     car['needRepair'] = true
//    console.log('Need Repair')
// } else {
//     car['needRepair'] = false
// }
// }

// check(car);

/**
 * Завдання 2.
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */
// const user2 = {
//   firstName: "Walter",
//   lastName: "White",
//   job: "Programmer",
//   pets: {
//     cat: "Kitty",
//     dog: "Doggy",
//   },
// };
// function check(obj) {
    //     for (let key in obj) {
    //         console.log(key, obj[key]);
    //
    //         if (obj.hasOwnProperty(key) && typeof obj[key] === 'object') {
    //             for (let key2 in obj[key]) {
    //                 console.log(key2, obj[key][key2]);
    //             }
    //         }
    //     }
    //
    // }
    //
    // check(user)



//   let arr = Object.entries(user2);
//   for (let i = 0; i < arr.length; i++) {
//     if (typeof arr[i][1] === "object") {
//       console.log(arr[i][1]);
//       console.log(Object.values(arr[i][1]))
//     }
//   }
// }

// if (Object.hasOwnProperty(key)) {
//     const element = user2[key];
//     console.log(typeof element)
//     if (typeof element === "object") {
//         for (const key in element) {
//             console.log(Object.entries(element))
//         }
//     }

// }
