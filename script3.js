//todo Conditional operators, switch
/* 
 Теоретичні питання
 1. Що таке логічний оператор?
 Логічні оператори використовуються для виконання логічних операцій над значеннями та виразами. Вони дозволяють приймати рішення на основі певних умов.


 2. Які логічні оператори є в JavaScript і які їх символи?
    -  && - і
    -  || - або
    - ! - не
    - !! - подвійна негація
 */

//  Практичні завдання.
//  1. Попросіть користувача ввести свій вік за допомогою prompt.
//  Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
//  що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
//  що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

//  Завдання з підвищенною складністю.
//  Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.

const age = prompt("Enter your age, please");
if (age === "" || isNaN(age)) {
    alert("You entered not a number");
} else {
    if(age < 12){
        alert("You are child");
    } else if( age < 18) {
        alert("You are teenager");
    } else {
        alert("You are old");
    }
}


// if (age === "") {
//   alert("You entered nothink");
// } else if (isNaN(age)) {
//   alert("You entered not a number");
// } else if (age < 12 && age >= 0) {
//   alert("You are child");
// } else if (age < 18 && age >= 12) {
//   alert("You are teenager");
// } else {
//   alert("You are old");
// }

//  2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
//  скільки днів у цьому місяці. Результат виводиться в консоль.
//  Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
//  (Використайте switch case)

// const month = prompt("Enter the month of the year").toLowerCase();
// let countDays = 0;
// const days28 = `${month} має 28 або 29 днів`
// const days30 = `${month} має 30 днів`
// const days31 = `${month} має 31 днів`

// switch (month) {
//   case "січень":
//     alert(days31);
//     break;
//   case "лютий":
//     alert(days28);
//     break;
//   case "березень":
//     alert(days31);
//     break;
//   case "квітень":
//     alert(days30);
//     break;
//   case "травень":
//     alert(days31);
//     break;
//   case "червень":
//     alert(days30);
//     break;
//   case "липень":
//     alert(days31);
//     break;
//   case "серпень":
//     alert(days31);
//     break;
//   case "вересень":
//     alert(days30);
//     break;
//   case "жовтень":
//     alert(days31);
//     break;
//   case "листопад":
//     alert(days30);
//     break;
//   case "грудень":
//     alert(days31);
//     break;
//   default:
//     {
//       alert("Такий місяць не визначено");
//     }
//     break;
// }


//ще варіант рішення
// const month = prompt("Enter the month of the year").toLowerCase();
// const days28 = `${month} має 28 або 29 днів`
// const days30 = `${month} має 30 днів`
// const days31 = `${month} має 31 день`
// const days28Test = "лютий".includes(month)
// const days31Test = "січень березень травень липень серпень жовтень грудень".includes(month)
// const days30Test = "квітень червень вересень листопад".includes(month)

// console.log(days28Test)
// console.log(days31Test)
// console.log(days30Test)

// let daysType = '';

// if (days31Test) {
//   daysType = '31';
// } else if (days28Test) {
//   daysType = '28';
// } else if (days30Test) {
//   daysType = '30';
// } else {
//   daysType = 'unknown';
// }
// switch (daysType) {
//   case "31":
//     alert(days31);
//     break;
//   case "28":
//     alert(days28);
//     break;
//     case "30":
//     alert(days30);
//     break;
//   default:
//     {
//       alert("Такий місяць не визначено");
//     }
//     break;
// }

