//? ARRAYS

//! HOME WORK

//  Теоретичні питання
//  1. Опишіть своїми словами як працює метод forEach.
// перебирає кожен елемент масиву, і виконує на основі цих даних щось. Він не змінює масив.

//  2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
// повертають новий масив:
//  - map 
//  - filter
//  - slice
// - reduce
//  - reverse
//  - sort
//-concat
// - every
//- some
// -includes
// - find
// -indexOf
// - findIndex

//  мутує масив:
  
//  - splice
//  - shift
//  - unshift
//  - push
//  - pop



//  3. Як можна перевірити, що та чи інша змінна є масивом?
//   Array.isArray([])


//  4. В яких випадках краще використовувати метод map(), а в яких forEach()?
// map() - якщо треба отримати новий масив зі новими/зміненими даними
//forEach() - коли треба коли не треба отримати новий масив, а просто перебрати і виконати якісь дії з елементами масиву, не міняючи масив. Наприклад, просто вивести елементи в консоль.


//  Практичні завдання
//  1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
const arr = ["travel", "hello", "eat", "ski", "lift"];
let longStr = 0;
arr.forEach((item) => {
  if (item.length > 3) {
    longStr++;
  }
});
console.log("task1 =", longStr);

//  2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//  Відфільтрований масив виведіть в консоль.
const arr2 = [
  { name: "Іван", age: 25, sex: "чоловіча" },
  { name: "Петро", age: 40, sex: "чоловіча" },
  { name: "Олена", age: 67, sex: "жіноча" },
  { name: "Дмитро", age: 17, sex: "чоловіча" },
];

const newArr2 = arr2.filter((item) => item.sex === "чоловіча");

console.log(newArr2);

//  3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

// Технічні вимоги:

// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

function filterBy(arr, type) {
    console.log(type)
   return arr.filter((item)=>{
        console.log(typeof item, item)
        if( item === null && type === 'null'){
            console.log(item === null)
            return false
        } else 
        if (typeof item !== type) {
            return true;
        }
        return false;
    })
}

const data1 = [
    "hello",
    33,
    "+",
    true,
    null,
    "world",
    false,
    22,
    "23",
    11,
    44,
    "hello",
    null,
  ];


console.log(filterBy(data1, 'boolean'));
console.log(filterBy(data1, 'null'));
console.log(filterBy(data1, 'string'));
console.log(filterBy(data1, 'number'));









//! CLASS WORK

/** Завдання - 1
 * На основі масиву map і масиву users зібрати новий масив об'єктів,
 * де в кожному об'єкті будуть лише ті властивості, які перераховані в масиві map.
 */

const map = ["_id", "name", "isActive", "balance"];
const users = [
  {
    _id: "5d220b10e8265cc978e2586b",
    isActive: true,
    balance: 2853.33,
    age: 20,
    name: "Buckner Osborne",
    gender: "male",
    company: "EMPIRICA",
    email: "bucknerosborne@empirica.com",
    phone: "+1 (850) 411-2997",
    registered: "2018-08-13T04:28:45 -03:00",
  },
  {
    _id: "5d220b10144ef972f6c2b332",
    isActive: true,
    balance: 1464.63,
    age: 38,
    name: "Rosalie Smith",
    gender: "female",
    company: "KATAKANA",
    email: "rosaliesmith@katakana.com",
    phone: "+1 (943) 463-2496",
    registered: "2016-12-09T05:15:34 -02:00",
  },
  {
    _id: "5d220b1083a0494655cdecf6",
    isActive: false,
    balance: 2823.39,
    age: 40,
    name: "Estrada Davenport",
    gender: "male",
    company: "EBIDCO",
    email: "estradadavenport@ebidco.com",
    phone: "+1 (890) 461-2088",
    registered: "2016-03-04T03:36:38 -02:00",
  },
];

const newArr = users.map((item) => {
  console.log(item);
  let obj = {};
  map.forEach((key) => {
    if (item.hasOwnProperty(key)) {
      // if(key in item)
      obj[key] = item[key];
    }
  });
  return obj;
});

console.log(newArr);

/** Завдання - 2
 * Дано масив об'єктів, де кожен об'єкт містить інформацію про літеру та місце її положення у рядку
 * {літера: “a”, позиція_в_пропозиції: 1}:
 * Напишіть функцію, яка з елементів масиву збере і поверне рядок, спираючись на index кожної літери. Наприклад:
 * [{char:"H",index:0}, {char:"i",index: 1}, {char:"!",index:2}] → "Hi!"
 */

const data = [
  { char: "a", index: 12 },
  { char: "w", index: 8 },
  { char: "Y", index: 10 },
  { char: "p", index: 3 },
  { char: "p", index: 2 },
  { char: "N", index: 6 },
  { char: " ", index: 5 },
  { char: "y", index: 4 },
  { char: "r", index: 13 },
  { char: "H", index: 0 },
  { char: "e", index: 11 },
  { char: "a", index: 1 },
  { char: " ", index: 9 },
  { char: "!", index: 14 },
  { char: "e", index: 7 },
];

function createString(data) {
  const sort = data.sort((a, b) => {
    return a.index - b.index;
  });
  console.log(sort);
  let str = "";
  sort.map((item) => {
    str += item.char;
  });
  console.log(str);
  return str;
}
createString(data);
// через редюсер зробити ще

const copy = [...users];





  
  