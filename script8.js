//? WORKING WITH STRINGS, DATE AND TIMES

//!  home work
//  Теоретичні питання
//  1. Як можна створити рядок у JavaScript?
// присвоїти змінній пустий або не пустий рядок
let stringNew = ''
// констректор new String() генерує рядок як обʼєкт, але якщо викликається як функція let str = String('Hello'), то значення приводиться до примітивного рядку

//  2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
// для запису рядку можна використовувати одинарні '' або подвійні "" лапки, або зворотні ``.
// зворотні `` лапки використовуються для запису шаблонних літералів, коли треба використати змінні безпосередньо в рядку

//  3. Як перевірити, чи два рядки рівні між собою?
// рядки порівнюються посимвольно, а для порівняння береться код кожного символу за таблюцею ASCII
// якщо чи однакові рядки - перебрати кожен символ рядку і порівняти

//  4. Що повертає Date.now()?
 // повертає кількість мілісекунд від 1.01.1970 00:00:00 по UTC в якості числа.

//  5. Чим відрізняється Date.now() від new Date()?

console.log(Date.now())  // повертає кількість мілісекунд від 1.01.1970 00:00:00 по UTC в якості числа.
console.log(new Date())  // повертає поточну дату на час в якості рядку


// Практичні завдання 
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом 
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

function isPalindrome(str){
    if(str.length === 0){
        console.log('string is empty')
        return 
    }
    let newStr = ''
    for (let i=str.length-1; i >=0 ; i--){
        newStr +=str[i]
        console.log(newStr)
    }
    if (newStr === str) {
        return true
    }
    return false
}
isPalindrome("")
isPalindrome("okko")
isPalindrome("okok")


// 2. Створіть функцію, яка  перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// // Рядок коротше 20 символів
// funcName('checked string', 20); // true
// // Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false
function checkLengthString(str, maxLength){
    if (!(str && maxLength)) {
        console.log('have not all arguments')
        return null
    }
   if(str.length <= maxLength){
    return true
   } 
   return false
}
console.log(checkLengthString('checked string')) // null
console.log(checkLengthString('checked string', 20)) // true
console.log(checkLengthString('checked string', 10)) //  false



// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.

console.log(Math.floor(0.01))

  let  ageUser = prompt('Enter your birthday in day/month/year format')
function checkAge(ageUser){
    const dateRegex = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/;
    // console.log(ageUser.search(dateRegex))
    if( !dateRegex.test(ageUser) || ageUser === null || ageUser === ''){
        // console.log('yes')
        ageUser = prompt('Enter your birthday in day/month/year format !')
        checkAge(ageUser)
    }
    let birthday = ageUser.split('/')
    const day = birthday[0]
    const month = birthday[1]-1
    const year = birthday[2]
    const dayBorn = new Date(year, month, day)
    // console.log(dayBorn)
    const dayToday = new Date()
    console.log('dayToday', dayToday)
    if(dayBorn > dayToday) {
        // console.log('No')
        ageUser = prompt('Enter your valid birthday in day/month/year format !')
        checkAge(ageUser)
    } else {
        // console.log((dayToday-dayBorn)/(1000 * 60 *60 * 24 * 365.25))
        // const getAge =  Math.floor((dayToday-dayBorn)/(1000 * 60 * 60 * 24 * 365.25))
        // console.log(day, month, year, getAge + " years old")

        let age = dayToday.getFullYear() - dayBorn.getFullYear();
        const monthDiff = dayToday.getMonth() - dayBorn.getMonth();
        const dayDiff = dayToday.getDate() - dayBorn.getDate();
        
        if (monthDiff < 0 || (monthDiff === 0 && dayDiff < 0)) {
            age--;
        }
        console.log(day, month, year, age + " years old");
        alert(`You are ${age} years old`);
        return age;

    }
}

console.log(checkAge(ageUser))







//! class work
// let str = 'Yes no yes'
// console.log('yes'.toLocaleUpperCase());

/**
  * Завдання - 1
  * Зробити першу та останню літери рядка у верхньому регістрі.
  *  За допомогою конкатенації та через шаблонні рядки.
 */
let string = 'some test string';
let firstIndex = string[0].toUpperCase()
 let midl = string.slice(1, string.length-1)
let lastIndex = string[string.length-1].toUpperCase()
let result =  firstIndex  + midl +lastIndex

// console.log(result);

/**
  * Завдання - 2
  * Знайти положення слова 'string' у рядку.
 */
let str = 'some test string';
let search = str.indexOf('test')
// console.log(search)

/**
  * Завдання - 3
  * Знайти положення другого пробілу в рядку ("вручну" нічого не рахувати).
 */

let search2 = str.indexOf(' ')
// console.log(search2);

let search3 = str.indexOf(' ', search2+1)
// console.log(search3);


/**
* Завдання - 4
* Получить строку с 5-го по 9-й символы.
*/

let result1 = str.slice(5, 9)
// console.log(result1);

/**
 * Завдання 5.
 *
 * Написати імплементацію вбудованої функції рядка repeat(times).
 *
 * Функція повинна мати два параметри:
 * - Цільовий рядок для пошуку символу за індексом;
 * - Кількість повторень цільового рядка.
 *
 * Функція повинна повертати перетворений рядок.
 *
 * Умови:
 * - Генерувати помилку, якщо перший параметр не є рядком, а другий не числом.
 */

function repeatStr(str, count){
    if(isNaN(count)){
        return
    }
    if(typeof str !== 'string') {
        return
    }
    let result = ""
    for (let i = 0; i < count; i++){
        result += str
    }
    return result
}

// console.log(repeatStr('str', 4))

/**
 * Завдання 6.
 *
 * Написати функцію, capitalizeAndDoublify, яка перекладає
 * символи рядка у верхній регістр і дублює кожен символ.
 *
 * Умови:
 * - Використовувати вбудовану функцію repeat;
 * - Використовувати вбудовану функцію toUpperCase;
 * - Використовувати цикл for...of.
 */

function capitalizeAndDoublify(str) {
    let result = ''
    for (let n of str) {
        // console.log(n)
        if(typeof n === 'string'){
            result += n.toUpperCase().repeat(2)
        } 

    }
    return result
}
// console.log(capitalizeAndDoublify('test string'))

/**
 * Завдання 7.
 *
 * Написати функцію truncate, яка «обрізає» занадто довгий рядок, додавши в кінці три крапки «...».
 *
 * Функція має два параметри:
 * - Вихідний рядок для обрізки;
 * - Допустима довжина рядка.
 *
 * Якщо рядок довший, ніж його допустима довжина — його необхідно обрізати,
 * і конкатенувати до кінця символ три крапки так, щоб разом з ним довжина
 * Рядки дорівнювала максимально допустимої довжині рядка з другого параметра.
 *
 * Якщо рядки не довші, ніж її допустима довжина.
 */

function truncate(str, count){
    if(str.length < count) {
        return str
    } 
    return str.slice(0, count-3) + '...'
}

// console.log(truncate('some test string', 10))

const now = new Date()
// console.log(now);
const date = new Date(1970, 10, 10)
// console.log(date)