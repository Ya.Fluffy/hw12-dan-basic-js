/*
    * Завдання 1
    * - Організувати функцію getInfo, яка приймає об'єкт виду
    * - { name: ..., info: { employees: [...], partners: [ … ] } }
    * - і виводить у консоль ім'я (якщо імені немає, показувати 'Unknown')
    * - і перші дві компанії з масиву partners:
    * - Якщо об'єкт пустий то функція повинна припинити свое виконання
 */

const organisation = {
    name: 'Google',
    info: {
        employees: ['Vlad', 'Olga'],
        partners: ['Microsoft', 'Facebook', 'Xing']
    }
};

const obj2 = {}
function getInfo(obj){
    let keys = Object.keys(obj).length
    console.log(keys)
    if(keys){
        if (obj["name"]){
            console.log('Name:', obj["name"])
            } else {
                console.log('Unknown')
            }

        for(let key in obj){
            if(typeof obj[key] === 'object'){
                console.log(obj[key])
                if(obj[key]['partners']){
                    let patners = Object.values(obj[key]['partners'])
                    console.log('Patners:', patners.slice(0, 2))
                }
            } else {
                console.log(obj[key])
            }
        }
    } else{
        console.log('no')
        return
    }

   
}

getInfo(organisation)
getInfo(obj2)


/*
    * Завдання 3
    * зробити геттер який буде повертати brand і model у вигляді рядка "Apple iPhone 7"
    * а також зробити сеттер в який буде передаватися рядок наприклад "Samsung S8 Gold"
    *  і в об'єкті в полі brand буде записано "Samsung" а в полі model буде записано "S8 Gold"
 */
const product = {
    brand: 'Apple',
    model: 'iPhone 7',
    price: '$300',

    set brandModel(newBrand){
        let info = newBrand.split(' ')
        console.log(info)
        this.brand = info[0]
        this.model = `${info[1]} ${info[2]}`
    },
    get brandModel(){
        return `${this.brand} ${this.model}`
    }
};



console.log(product.brandModel)
product.brandModel = 'Sumsung S8 Gold'
console.log(product.brandModel)

/**
 * Завдання 4
 *
 * Створити об'єкт користувача, який має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Умови:
 * - Всі властивості мають бути не доступні для запису;
 * - Розв'язати задачу двома способами.
 */

const user = {}
Object.defineProperties(user, {
 name:{
    value: "Name",
    writable: false
 },
 lastName: {
    value: "lastName",
    writable: false
 },
 prof: {
    value: "profession",
    writable: false

 }
})

console.log(user)

const user2 = {
    name: 'Name',
    lastName: 'LastName',
    prof: null
}

Object.freeze(user2)


/**
 * Завдання 5.
 *
 * Написати функцію-фабрику, яка повертає об'єкти користувачів.
 *
 * Об'єкт користувача має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Функція-фабрика в свою чергу має три параметри,
 * які відбивають вищеописані властивості об'єкта.
 * Кожен параметр функції має значення за замовчуванням: null.
 */


function createUser(name = null, lastName = null, job = null){
    return { name, lastName, job
    }
}

console.log(createUser('Piter', 'Piterson', 'qa'))